package com.example.testgps.model;

public class Vehiculo {
    private String latitud;
    private String Longitud;

    public Vehiculo(String latitud, String longitud) {
        this.latitud = latitud;
        Longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }
}
