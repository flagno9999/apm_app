package com.example.testgps.model;

public class Viaje {
    private Integer conductor;
    private Integer currentLat;
    private Integer currentLng;
    private String destino;
    private Integer estado;
    private Integer latitud;
    private Integer longitud;
    private String viaje_id;

    public Viaje() {
    }

    public Viaje(Integer conductor, Integer currentLat, Integer currentLng, String destino, Integer estado, Integer latitud, Integer longitud, String viaje_id) {
        this.conductor = conductor;
        this.currentLat = currentLat;
        this.currentLng = currentLng;
        this.destino = destino;
        this.estado = estado;
        this.latitud = latitud;
        this.longitud = longitud;
        this.viaje_id = viaje_id;
    }

    public Integer getConductor() {
        return conductor;
    }

    public void setConductor(Integer conductor) {
        this.conductor = conductor;
    }

    public Integer getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(Integer currentLat) {
        this.currentLat = currentLat;
    }

    public Integer getCurrentLng() {
        return currentLng;
    }

    public void setCurrentLng(Integer currentLng) {
        this.currentLng = currentLng;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getLatitud() {
        return latitud;
    }

    public void setLatitud(Integer latitud) {
        this.latitud = latitud;
    }

    public Integer getLongitud() {
        return longitud;
    }

    public void setLongitud(Integer longitud) {
        this.longitud = longitud;
    }

    public String getViaje_id() {
        return viaje_id;
    }

    public void setViaje_id(String viaje_id) {
        this.viaje_id = viaje_id;
    }
}
